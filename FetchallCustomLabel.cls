public class FetchallCustomLabel {
	public static string finalCSVString ='Id,Name,Value\n';
    
    //variables to parse customlabel json response
    public Integer size;
    public Integer totalSize;
    public boolean done;
    public String nextRecordsUrl;	
    public String queryLocator;	
    public String entityTypeName;	
    public List<records> records;
    class records {
        public String Id;	
        public String Name;	
        public String Value;	
    }
    
    public static void fetchAllCustomLabels(){
        findAllCustomLabels('');
    }
    
    public static void findAllCustomLabels(string endpointURL){
        if(endpointURL=='' || endpointURL==null){
            endpointURL='/services/data/v45.0/tooling/query/?q=Select+id,Name,Value+from+CustomLabel';
        }
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        req.setHeader('Content-Type', 'application/json');
        String domainUrl=URL.getSalesforceBaseUrl().toExternalForm();
        system.debug('********domainUrl:'+domainUrl);
        req.setEndpoint(domainUrl+endpointURL);
        req.setMethod('GET');
        
        Http h = new Http();
        HttpResponse res = h.send(req);
        string responseBody = res.getBody();
        system.debug('****responseBody:'+responseBody); 
        
        SK_CustomlabelUtility completeWrapper = (SK_CustomlabelUtility)System.JSON.deserialize(responseBody, SK_CustomlabelUtility.class);
        responseBody = null; //make this variable as null to free up heap memory allocation
        res = null;        
        string cname,cvalue;
        for(records rec: completeWrapper.records){
            cname= rec.Name;
            cvalue= rec.Value;
            cname = cname.escapeCsv();
            cvalue = cvalue.escapeCsv();
            finalCSVString = finalCSVString + rec.Id+','+cname+','+cvalue+'\n';
        }
        boolean isMoreRecordsAvailable =  completeWrapper.done;
        string nextEndPointUrl= completeWrapper.nextRecordsUrl;
        completeWrapper = null; //make this variable as null to free up heap memory allocation
        
        
        if(!isMoreRecordsAvailable){
            system.debug('****Total Allowed Callout Limits-'+Limits.getLimitCallouts());
            system.debug('****No.of callout performed-'+Limits.getCallouts());
            if(Limits.getLimitCallouts()-Limits.getCallouts()>0){
            	findAllCustomLabels(nextEndPointUrl);    
            }
        }else{
            List<string> toAddresses= new List<string>();
            sendEmail(finalCSVString,'All Custom Labels.csv','Custom Label Information','Please find attached file for your reference',toAddresses);
        }
        
    }
    
    
    public static void sendEmail(string csvString, string csvFileName,string emailSubject,string emailBody, List<string> toAddresses){
        Messaging.EmailFileAttachment csvAtt = new Messaging.EmailFileAttachment();
		    blob csvBlob = Blob.valueOf(csvString);
        string csvname= csvFileName;
		    csvAtt.setFileName(csvFileName);
		    csvAtt.setBody(csvBlob);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if(toAddresses.size()==0){
            string currentUserEmailId = UserInfo.getUserEmail();
            toAddresses.add(currentUserEmailId);
        }if(emailBody=='' || emailBody==null){
        	emailBody = 'Please find attached file for your reference.' ;  
        }
        mail.setToAddresses(toAddresses);
        mail.setSubject(emailSubject);  
        mail.setPlainTextBody(emailBody);
        mail.setHtmlBody(emailBody);  
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAtt});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}